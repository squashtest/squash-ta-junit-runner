/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.squash.ta.junit.runner.maven.json.listing;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.squashtest.ta.api.test.toolkit.ExpectedCollections;
import org.squashtest.ta.galaxia.squash.ta.junit.runner.factory.TestPointer;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.TestDescriptor;

/**
 * @author edegenetais
 */
public class JunitRunnerJsonTestTreeTest extends AbstractTestPointerRelatedTest{
    private File destDir;
    private JunitRunnerJsonTestTree testee;
    
    @Before
    public void setUp() throws IOException{
        destDir=createNtrackDir();
        testee=new JunitRunnerJsonTestTree(destDir);
        testee.setDateFactory(new JunitRunnerJsonTestTree.DateFactory() {
            @Override
            public Date getCurrentDate() {
                // first of Jan 1970 is THE DATE !
                return new Date(0);
            }
        });
    }
    
    @Test
    public void oneEcosystemTwoTestsNoMetadata() throws IOException{
        Map<String,List<TestPointer>> testRefByGroups=createTwoTestEcosystem();
        
        testee.write(testRefByGroups, false);
        
        File expectedJson=createFile("expected-oneecosystem-no-metadata.json");
        
        checkActualContentAgainstExpected(new File(destDir,"test-tree/testTree.json"),expectedJson);
    }
    
    @Test
    public void twoEcosystemFiveTestsNoMetadata() throws IOException{
        Map<String,List<TestPointer>> testRefByGroups=createTwoTestEcosystem();
        addEcosystem("baseBundle", "my.test.OtherClass", testRefByGroups, "firstOtherTest","secondOtherTest","lastOtherTest");
        
        testee.write(testRefByGroups, false);
        
        File expectedJson=createFile("expected-twoecosystems-no-metadata.json");
        
        checkActualContentAgainstExpected(new File(destDir,"test-tree/testTree.json"),expectedJson);
    }
    
    @Test
    public void oneEcosystemTwoTestsEmptyMetadata() throws IOException{
        Map<String,List<TestPointer>> testRefByGroups=createTwoTestEcosystem();
        
        testee.write(testRefByGroups, true);
        
        File expectedJson=createFile("expected-oneecosystem-empty-metadata.json");
        
        checkActualContentAgainstExpected(new File(destDir,"test-tree/testTree.json"),expectedJson);
    }
    
    @Test
    public void twoEcosystemSixTestsWithMetadata() throws IOException{
        Map<String,List<TestPointer>> testRefByGroups=createTwoTestEcosystem();
        final String bundleName = "baseBundle";
        final String className = "my.test.OtherClass";
        addEcosystem(bundleName, className, testRefByGroups, "firstOtherTest","secondOtherTest","lastOtherTest");
        TestDescriptor descriptor=createDescriptor(className, "thirdOtherTest", createUniqueId());
        
        final ArrayList metadataList = new ArrayList();
        final Map<String,List<String>> mdRegister1 = new HashMap();
        mdRegister1.put("double", ExpectedCollections.expectedList("value1","value2"));
        mdRegister1.put("unique", ExpectedCollections.expectedList("uniqueValue"));
        metadataList.add(mdRegister1);
        
        final Map<String,List<String>> mdRegister2 = new HashMap();
        mdRegister2.put("double", ExpectedCollections.expectedList("value2","value3"));
        mdRegister2.put("emptyKeyCase", new ArrayList<String>(0));
        metadataList.add(mdRegister2);
        
        Mockito.when(descriptor.metadataList()).thenReturn(metadataList);
        
        testRefByGroups.get(cookEcosystemName(bundleName, className)).add(2, new TestPointer(descriptor, bundleName));
        
        testee.write(testRefByGroups, true);
        
        File expectedJson=createFile("expected-twoecosystems-with-metadata.json");
        
        checkActualContentAgainstExpected(new File(destDir,"test-tree/testTree.json"),expectedJson);
    }

    private Map<String, List<TestPointer>> createTwoTestEcosystem() {
        Map<String,List<TestPointer>> testRefByGroups=new HashMap<>();
        addEcosystem("baseBundle", "my.test.Class",testRefByGroups,"firstTest","secondTest");
        return testRefByGroups;
    }

}
