/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.squash.ta.junit.runner.maven.json.listing;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import org.junit.Test;
import org.squashtest.ta.api.test.toolkit.ExpectedCollections;
import org.squashtest.ta.galaxia.squash.ta.junit.runner.factory.TestPointer;

/**
 *
 * @author edegenetais
 */
public class TestPointerManagerTest extends AbstractTestPointerRelatedTest{
    @Test
    public void testSimpleSingleEcosystem(){
        final String testClass = "my.TestClass";
        final String bundleName = "main";
        List<TestPointer> testSpecs=ExpectedCollections.expectedList(cookTestDescriptorMock(testClass, bundleName, "firstTest"),
                cookTestDescriptorMock(testClass, bundleName, "secondTest")
        );
        TestPointerManager testee=new TestPointerManager(testSpecs);
        Map<String,List<TestPointer>> result=testee.groupPointersByEcosystemDefinition();
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(bundleName+":"+testClass, result.keySet().iterator().next());
        Assert.assertEquals(new HashSet<>(testSpecs), new HashSet<>(result.values().iterator().next()));
    }
    
    @Test
    public void twoTestsTwoBundlesTwoEcosystems(){
        final String testClass = "my.TestClass";
        final String main = "main";
        final TestPointer testPointerFirstTest = cookTestDescriptorMock(testClass, main, "firstTest");
        final String pied = "pied";
        final TestPointer testPointerSecondTest = cookTestDescriptorMock(testClass, pied, "secondTest");
        List<TestPointer> testSpecs=ExpectedCollections.expectedList(testPointerFirstTest, testPointerSecondTest);
        
        TestPointerManager testee=new TestPointerManager(testSpecs);
        Map<String,List<TestPointer>> result=testee.groupPointersByEcosystemDefinition();
        Assert.assertEquals(2, result.size());
        final String mainName = main+":"+testClass;
        final String piedName = pied+":"+testClass;
        Assert.assertEquals(ExpectedCollections.expectedSet(mainName, piedName)
                , result.keySet());
        final List<TestPointer> mainList = result.get(mainName);
        Assert.assertEquals(1, mainList.size());
        Assert.assertTrue(mainList.get(0).equals(testPointerFirstTest));
        
        final List<TestPointer> piedList = result.get(piedName);
        Assert.assertEquals(1, piedList.size());
        Assert.assertTrue(piedList.get(0).equals(testPointerSecondTest));
    }
    
    @Test
    public void twoTestsTwoClassesTwoEcosystems(){
        final String testClass = "my.TestClass";
        final String usClass = "my.AmericanClass";
        final String mainBundle = "main";
        final TestPointer testPointerFirstTest = cookTestDescriptorMock(testClass, mainBundle, "firstTest");
        final TestPointer testPointerSecondTest = cookTestDescriptorMock(usClass, mainBundle, "secondTest");
        List<TestPointer> testSpecs=ExpectedCollections.expectedList(testPointerFirstTest, testPointerSecondTest);
        
        TestPointerManager testee=new TestPointerManager(testSpecs);
        Map<String,List<TestPointer>> result=testee.groupPointersByEcosystemDefinition();
        Assert.assertEquals(2, result.size());
        final String normalName = mainBundle+":"+testClass;;
        final String usName = mainBundle+":"+usClass;
        Assert.assertEquals(ExpectedCollections.expectedSet(normalName, usName)
                , result.keySet());
        final List<TestPointer> normalList = result.get(normalName);
        Assert.assertEquals(1, normalList.size());
        Assert.assertTrue(normalList.get(0).equals(testPointerFirstTest));
        
        final List<TestPointer> usList = result.get(usName);
        Assert.assertEquals(1, usList.size());
        Assert.assertTrue(usList.get(0).equals(testPointerSecondTest));
    }
}
