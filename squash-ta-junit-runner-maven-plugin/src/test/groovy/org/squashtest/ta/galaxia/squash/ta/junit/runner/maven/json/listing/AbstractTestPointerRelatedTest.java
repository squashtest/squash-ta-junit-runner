/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.squash.ta.junit.runner.maven.json.listing;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.mockito.Mockito;
import org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase;
import org.squashtest.ta.galaxia.squash.ta.junit.runner.factory.TestPointer;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.TestDescriptor;

/**
 *
 * @author edegenetais
 */
public class AbstractTestPointerRelatedTest extends ResourceExtractorTestBase {

    private int lastId=0;
    
    protected static String cookEcosystemName(final String bundleName, final String className) {
        return bundleName + ":" + className;
    }

    protected void addEcosystem(final String bundleName, final String className, Map<String, List<TestPointer>> testRefByGroups, String... testNames) {
        final ArrayList<TestPointer> ecosystemTests = new ArrayList<>();
        for (String oneTestName : testNames) {
            ecosystemTests.add(cookTestDescriptorMock(className, bundleName, oneTestName));
        }
        testRefByGroups.put(cookEcosystemName(bundleName, className), ecosystemTests);
    }

    protected TestPointer cookTestDescriptorMock(final String className, final String bundleName, String testName) {
        return new TestPointer(createDescriptor(className, testName, createUniqueId()), bundleName);
    }

    protected TestDescriptor createDescriptor(String className, String testName, String uniqueId) {
        final TestDescriptor descriptorMock = Mockito.mock(TestDescriptor.class);
        Mockito.when(descriptorMock.className()).thenReturn(className);
        Mockito.when(descriptorMock.displayName()).thenReturn(testName);
        Mockito.when(descriptorMock.uniqueId()).thenReturn(uniqueId);
        return descriptorMock;
    }

    protected String createUniqueId() {
        return Integer.toString(++lastId);
    }
    
}
