/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.squash.ta.junit.runner.maven;

import java.io.File;
import java.util.List;
import java.util.Map;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.squashtest.ta.galaxia.squash.ta.junit.runner.explorer.JUnit5TestExplorer;
import org.squashtest.ta.galaxia.squash.ta.junit.runner.factory.Junit5TestWorkspaceBrowser;
import org.squashtest.ta.galaxia.squash.ta.junit.runner.factory.TestPointer;
import org.squashtest.ta.galaxia.squash.ta.junit.runner.maven.json.listing.JunitRunnerJsonTestTree;
import org.squashtest.ta.galaxia.squash.ta.junit.runner.metadata.JunitRunnerMetadataSyntaxChecker;
import org.squashtest.ta.galaxia.squash.ta.junit.runner.maven.json.listing.TestPointerManager;
import org.squashtest.ta.maven.AbstractBaseSquashTaMojo;
import org.squashtest.ta.plugin.commons.resources.ThirdPartyJavaCodeBundle;
import org.squashtest.ta.squash.ta.plugin.junit.resources.JunitTestBundle;


/**
 * Mojo to list junit tests in a junit project.
 * 
 * @author fgautier
 *
 * @goal list
 * @phase integration-test
 * @requiresDependencyResolution test
 */
public class SquashTAJunitListMojo extends AbstractBaseSquashTaMojo {
    
    /*
     * XXX: as a result of the use of doclets in the base project
     * squash-ta-maven-plugin,and the incredibliy stupid decision of the maven
     * project to NOT implement x-doclet marking of get/set defined virtual
     * fields for the plugin manifest generator, we can't re-use declarations
     * from the superclass (unlike annotations, doclets are lost through
     * compilation), so we need to jump through hoops to get the work done, by
     * redefining all configuration here...
     */
    /**
	 * @parameter
	 * @deprecated workaround for the inheritance problem due to maven doclets real field in superclass
	 */
    @Deprecated
	@SuppressWarnings("unused")// this is FAKE, real field in superclass
	private File logConfiguration;

	/**
	 * @parameter property
	 * @deprecated workaround for the inheritance problem due to maven doclets real field in superclass
	 */
    @Deprecated
	@SuppressWarnings("unused")// this is FAKE, real field in superclass
	private File outputDirectory;
	
    /*
	 * END OF Workaround, after here we have *real* parameter declarations &
	 * code...
	 */
    
    /**
     * @parameter property="project.build.directory"
     */
    private File buildDirectory; 
    /**
     * @parameter property=project.compileClasspathElements
     */
    private List<String> compileCP;

    /**
     * @parameter property=project.testClasspathElements
     */
    private List<String> testCP;

    /**
     * @parameter property=project.build.outputDirectory
     */
    private File mainClasses;

    /**
     * @parameter property=project.build.testOutputDirectory
     */
    private File testClasses;
    
    /**
    * 
    * @parameter property="tf.disableMetadata"
    */
    private boolean disableMetadata;
    
    @Override
    public void setOutputDirectory(File outputDirectory) {
            super.setOutputDirectory(outputDirectory);
            this.outputDirectory=outputDirectory;
    }
    
    @Override
    protected void execution() throws MojoExecutionException, MojoFailureException {
        initLogging();
        init();
        resetOutputDirectory();
        
        getLogger().info("Listing Junit tests in main bundle \""+mainClasses.getName()+"\" and test bundle \""+ testClasses.getName() +"\".");        
        
        /* project analysis */
        final JunitTestBundle mainCodeBundle = new JunitTestBundle(new ThirdPartyJavaCodeBundle(compileCP,mainClasses));
        final JunitTestBundle testCodeBundle = new JunitTestBundle(new ThirdPartyJavaCodeBundle(testCP,testClasses));
        Junit5TestWorkspaceBrowser browser = new Junit5TestWorkspaceBrowser(mainCodeBundle, testCodeBundle);
        final JUnit5TestExplorer explorer = new JUnit5TestExplorer();
        List<TestPointer> availableTests = explorer.listAvailableTests(browser.getBundleMap());
        
        TestPointerManager testPointerManager = new TestPointerManager(availableTests);
        //grouping the test pointers by theirs file name
        Map<String, List<TestPointer>> testGroupMap = testPointerManager.groupPointersByEcosystemDefinition();
        
        getLogger().debug("Found '" + availableTests.size() + "' test(s)");
        
        /* jsonified test list export*/
        JunitRunnerJsonTestTree listing = new JunitRunnerJsonTestTree(getOutputDirectory());
                
        if (disableMetadata){
            listing.write(testGroupMap, false);
        } else{
            //metadata validation
            getLogger().info("Squash TF: Metadata checking...");
            new JunitRunnerMetadataSyntaxChecker(testGroupMap, true, true, false).checkMetadataSyntaxByDefault();
            getLogger().info("Squash TF: Metadata checking successfully completed");
             //json file generation
            listing.write(testGroupMap, true);
        }
        
    }

    private void init() {
        if(getOutputDirectory()==null) {
            setOutputDirectory(new File(buildDirectory, "squashTF"));
        }
    }
    
}
