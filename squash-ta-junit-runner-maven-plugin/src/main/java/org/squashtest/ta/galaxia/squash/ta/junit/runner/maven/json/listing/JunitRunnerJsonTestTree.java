/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.squash.ta.junit.runner.maven.json.listing;

import org.squashtest.ta.squash.ta.addon.logic.kit.AbstractRunnerJsonTree;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.squashtest.ta.galaxia.squash.ta.junit.runner.factory.TestPointer;
import org.squashtest.ta.squash.ta.plugin.junit.library.bridge.TestDescriptor;

/**
 * Implementation of the junit test to json list json transformation that creates the json test list used by Squash TM.
 * This class reimplemented the json codec from scratch. I (edegenetais) changed it to use a base class from galaxia-toolkit.
 * @author qtran
 */
public class JunitRunnerJsonTestTree extends AbstractRunnerJsonTree<TestPointer> {
    
    public JunitRunnerJsonTestTree(File destinationDir) {
        super(destinationDir);
    }

    @Override
    protected Map<String, List<String>> getMetadataForTestPointer(TestPointer testPointer) {
        Map<String,List<String>> metadataMap=new HashMap<>();
        for (TestDescriptor test : testPointer.getJunitTests()){
            
            extractJunitTestMetadata(test, metadataMap);
            
        }
        return metadataMap;
    }

    private void extractJunitTestMetadata(TestDescriptor test, Map<String, List<String>> metadataMap) {
        final List<Map<String, List<String>>> metadataList = test.metadataList();
        for(Map<String,List<String>> methodMetadataMap:metadataList){
            extractMetadataValuesForKey(methodMetadataMap, metadataMap);
        }
    }

    private void extractMetadataValuesForKey(Map<String, List<String>> methodMetadataMap, Map<String, List<String>> metadataMap) {
        for(String key:methodMetadataMap.keySet()){
            if(metadataMap.containsKey(key)){
                metadataMap.get(key).addAll(methodMetadataMap.get(key));
            }else{
                //Just inserting in the original metadata list may cause the next fusion to fail of it somehow happens to be unmodifiable => create a new list
                metadataMap.put(key,new ArrayList<>(methodMetadataMap.get(key)));
            }
        }
    }

    @Override
    protected String getTestPointerTestName(TestPointer testPointer) {
        return testPointer.simpleName();
    }
    
    /* 
     * This allows tests to work with reproducible date fields - thet replace the DateFactory
     * by an implementation with a hardwired constant date.
     */
    protected static interface DateFactory extends AbstractRunnerJsonTree.DateFactory{}
    protected void setDateFactory(DateFactory df){
        this.dateFactory=df;
    }
    /* End of date related testability code. */
}
