/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.squashtest.ta.galaxia.squash.ta.junit.runner.maven;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.galaxia.squash.ta.junit.runner.explorer.JUnit5TestExplorer;
import org.squashtest.ta.galaxia.squash.ta.junit.runner.factory.Junit5TestWorkspaceBrowser;
import org.squashtest.ta.galaxia.squash.ta.junit.runner.factory.TestPointer;
import org.squashtest.ta.galaxia.squash.ta.junit.runner.metadata.JunitRunnerMetadataSyntaxChecker;
import org.squashtest.ta.galaxia.squash.ta.junit.runner.maven.json.listing.TestPointerManager;
import org.squashtest.ta.maven.AbstractBaseSquashTaMojo;
import org.squashtest.ta.plugin.commons.resources.ThirdPartyJavaCodeBundle;
import org.squashtest.ta.squash.ta.plugin.junit.resources.JunitTestBundle;

/**
 * Squash TA goal in order to validate the syntax of all JUnit test method metadata 
 * that located in the test root dir of the test project.
 * 
 * @author qtran
 * 
 * @goal check-metadata
 * @phase integration-test
 * @requiresDependencyResolution test
 */
public class SquashTAJunitCheckMetadataMojo extends AbstractBaseSquashTaMojo{
    private static final Logger LOGGER = LoggerFactory.getLogger(SquashTAJunitCheckMetadataMojo.class);
    /**
     * @parameter property="project.build.directory"
     */
    private File buildDirectory;
    
    /**
     * @parameter property=project.build.outputDirectory
     */
    private File mainClasses;
    
    /**
     * @parameter property=project.build.testOutputDirectory
     */
    private File testClasses;
    
    /**
     * @parameter property=project.compileClasspathElements
     */
    private List<String> compileCP;

    /**
     * @parameter property=project.testClasspathElements
     */
    private List<String> testCP;
    
    /**
     * 
     * @parameter property="tf.metadata.check"
     */
    private String metadataCheckParam;
    
    /**
     * 
     * @parameter property="tf.metadata.check.keys"
     */
    private String metadataCheckKeyListParam;
    
    @Override
    protected void execution() throws MojoExecutionException, MojoFailureException {
        initLogging();
        init();
        resetOutputDirectory();
        
        getLogger().info("Listing Junit tests in main bundle \""+mainClasses.getName()+"\" and test bundle \""+ testClasses.getName() +"\".");  
        
        /* project analysis */
        final JunitTestBundle mainCodeBundle = new JunitTestBundle(new ThirdPartyJavaCodeBundle(compileCP, mainClasses));
        final JunitTestBundle testCodeBundle = new JunitTestBundle(new ThirdPartyJavaCodeBundle(testCP, testClasses));
        Junit5TestWorkspaceBrowser browser = new Junit5TestWorkspaceBrowser(mainCodeBundle, testCodeBundle);
        final JUnit5TestExplorer explorer = new JUnit5TestExplorer();
        List<TestPointer> availableTests = explorer.listAvailableTests(browser.getBundleMap());
        
        TestPointerManager testPointerManager = new TestPointerManager(availableTests);
        //group tests by its class
        Map<String, List<TestPointer>> testGroupMap = testPointerManager.groupPointersByEcosystemDefinition();
        
        getLogger().debug("Found '" + availableTests.size() + "' test(s)");
        
     
        
        if (null == metadataCheckParam){
            
            //********* execution ********
            JunitRunnerMetadataSyntaxChecker checker = new JunitRunnerMetadataSyntaxChecker(testGroupMap, true, true, false);
            getLogger().info("Squash TF: checking metadata...");
            
            //default metadata syntax validation
            checker.checkMetadataSyntaxByDefault();
            
        } else {
            
            //********* execution ********
            JunitRunnerMetadataSyntaxChecker checker = new JunitRunnerMetadataSyntaxChecker(testGroupMap, true, true, true);
            getLogger().info("Squash TF: checking metadata...");            
            
            LOGGER.debug("User input for tf.metadata.check: \'"+metadataCheckParam+"\'.");
            String userInputForMetadataCheckParam = metadataCheckParam.trim();            
            
            if (isWrappedByBrackets(userInputForMetadataCheckParam)){
                treatUserInputListForMetadataCheckParam(userInputForMetadataCheckParam, checker);
            } else {
                raiseNonCoveredByBracketsArgExcptForMetadataCheckParam();
            }
        }
        getLogger().info("Squash TF: Metadata checking successfully completed");
    }
    
    private boolean isWrappedByBrackets(String userInputForMetadataCheckParam) {
        String regex = "\\[.*\\]";
        Pattern pat = Pattern.compile(regex);
        return pat.matcher(userInputForMetadataCheckParam).matches();
    }
    
    private void treatUserInputListForMetadataCheckParam(String userInputForMetadataCheckParam, JunitRunnerMetadataSyntaxChecker checker) {
        String removedBracketsInput = removeBracketsOfInputString(userInputForMetadataCheckParam);
        String trimmedRemovedBracketsInput = removedBracketsInput.trim();
        if ("".equals(trimmedRemovedBracketsInput)){
            invokeEmptyUserInputListForMetadataCheckParamException();
        } else {
            treatNotEmptyUserInputListForMetadataCheckParam(removedBracketsInput, checker);
        }
    }
    
    private String removeBracketsOfInputString(String userInputForMetadataCheckParam) {
        return userInputForMetadataCheckParam.substring(1, userInputForMetadataCheckParam.length()-1);
    }
    
    private void invokeEmptyUserInputListForMetadataCheckParamException() {
        LOGGER.error("\'"+metadataCheckParam+"\': "+INVALID_METADATA_CHECK_PARAM
                +". Parameter input list MUST NOT be empty.");
        throw new IllegalArgumentException(INVALID_METADATA_CHECK_PARAM);
    }
    
    private void treatNotEmptyUserInputListForMetadataCheckParam(String removedBracketsInput, JunitRunnerMetadataSyntaxChecker checker) {
        String[] inputList = removedBracketsInput.split(",");
        for (String input :  inputList){
            String trimmedInput =  input.trim();
            if ("".equals(trimmedInput)){
                invokeEmptyUserInputForMetadataCheckParamException(trimmedInput);
            } else {
                treatUserInputForMetadataCheckParam(trimmedInput, checker);
            }
        }        
    }
    
    private void invokeEmptyUserInputForMetadataCheckParamException(String trimmedInput) {
        LOGGER.error("\'"+trimmedInput+"\': "+INVALID_METADATA_CHECK_PARAM
                +". Parameter input MUST NOT be empty.");
        throw new IllegalArgumentException(INVALID_METADATA_CHECK_PARAM);
    }
    
    private void treatUserInputForMetadataCheckParam(String trimmedInput, JunitRunnerMetadataSyntaxChecker checker) {
        switch (trimmedInput) {
            case "valueUnicity":
                getLogger().info("Squash TF: checking metadata with value unicity...");
                checker.checkMetadataSyntaxWithUnicityChecking(metadataCheckKeyListParam);
                break;
            case "keyUnicity":
                //TODO: Not implemented yet
                getLogger().info("Squash TF: checking metadata with key unicity...");
                break;
            default:
                raiseIllegalArgExcptForMetadataCheckParam(trimmedInput);
        }
    }
    
    private void raiseIllegalArgExcptForMetadataCheckParam(String trimmedInput) {
        LOGGER.error("\'"+trimmedInput+"\': "+INVALID_METADATA_CHECK_PARAM
                +". Parameter input is NOT defined.");
        throw new IllegalArgumentException(INVALID_METADATA_CHECK_PARAM);
    }
    
    private void raiseNonCoveredByBracketsArgExcptForMetadataCheckParam() {
        LOGGER.error("\'"+metadataCheckParam+"\': "+INVALID_METADATA_CHECK_PARAM
                +". Parameter input(s) must be wrapped in brackets, ex: \"[valueUnicity]\" or \"[valueUnicity, keyUnicity]\".");
        throw new IllegalArgumentException(INVALID_METADATA_CHECK_PARAM);
    }
    
    private void init() {
        if(getOutputDirectory()==null) {
            setOutputDirectory(new File(buildDirectory, "squashTF"));
        }
    }
    
}
