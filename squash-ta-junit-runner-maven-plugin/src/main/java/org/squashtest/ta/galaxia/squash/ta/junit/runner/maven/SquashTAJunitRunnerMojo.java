/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.squash.ta.junit.runner.maven;

import org.squashtest.ta.galaxia.squash.ta.junit.runner.factory.Junit5TestWorkspaceBrowser;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.squashtest.ta.backbone.init.EngineLoader;
import org.squashtest.ta.commons.exporter.ReportType;
import org.squashtest.ta.commons.exporter.ResultExporter;
import org.squashtest.ta.commons.exporter.html.HtmlSuiteResultExporter;
import org.squashtest.ta.commons.exporter.html.HtmlSummarySuiteResultExporter;
import org.squashtest.ta.commons.factories.specification.SuiteSpecification;
import org.squashtest.ta.framework.facade.Configurer;
import org.squashtest.ta.framework.facade.Engine;
import org.squashtest.ta.framework.test.definition.TestSuite;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.galaxia.squash.ta.junit.runner.factory.TestPointer;
import org.squashtest.ta.galaxia.squash.ta.junit.runner.factory.Junit5TestSuiteFactory;
import org.squashtest.ta.galaxia.squash.ta.junit.runner.factory.Junit5TestSuiteFactoryBuilder;
import org.squashtest.ta.galaxia.squash.ta.junit.runner.metadata.JunitRunnerMetadataSyntaxChecker;
import org.squashtest.ta.galaxia.squash.ta.junit.runner.link.JunitTestSpecparser;
import org.squashtest.ta.galaxia.squash.ta.junit.runner.maven.json.listing.TestPointerManager;
import org.squashtest.ta.galaxia.tf.param.service.ParametersFactory;
import org.squashtest.ta.galaxia.tf.param.service.TFParamServiceImpl;
import org.squashtest.ta.maven.AbstractSquashTaMojo;
import org.squashtest.ta.maven.TmCallBack;
import org.squashtest.ta.maven.utils.TAMojoExporterAdapter;
import org.squashtest.ta.plugin.commons.resources.ThirdPartyJavaCodeBundle;
import org.squashtest.ta.squash.ta.addon.logic.kit.TestInfoEventConfigurer;
import org.squashtest.ta.squash.ta.plugin.junit.resources.JunitTestBundle;



/**
 * @goal run
 * @phase integration-test
 * @requiresDependencyResolution test
 * 
 * @author edegenetais
 */
public class SquashTAJunitRunnerMojo extends AbstractSquashTaMojo{
    /*
     * XXX: as a result of the use of doclets in the base project
     * squash-ta-maven-plugin,and the incredibliy stupid decision of the maven
     * project to NOT implement x-doclet marking of get/set defined virtual
     * fields for the plugin manifest generator, we can't re-use declarations
     * from the superclass (unlike annotations, doclets are lost through
     * compilation), so we need to jump through hoops to get the work done, by
     * redefining all configuration here...
     */
	/**
	 * @parameter
	 * @deprecated workaround for the inheritance problem due to maven doclets real field in superclass
	 */
        @Deprecated
	@SuppressWarnings("unused")// this is FAKE, real field in superclass
	private Configurer[] configurers;

	/**
	 * @parameter
	 * @deprecated workaround for the inheritance problem due to maven doclets real field in superclass
	 */
        @Deprecated
	@SuppressWarnings("unused")// this is FAKE, real field in superclass
	private ResultExporter[] exporters;

        
	/**
	 * @parameter
	 * @deprecated workaround for the inheritance problem due to maven doclets real field in superclass
	 */
        @Deprecated
	@SuppressWarnings("unused")// this is FAKE, real field in superclass
	private File logConfiguration;

	/**
	 * @parameter property
	 * @deprecated workaround for the inheritance problem due to maven doclets real field in superclass
	 */
        @Deprecated
	@SuppressWarnings("unused")// this is FAKE, real field in superclass
	private File outputDirectory;

        /*
	 * END OF Workaround, after here we have *real* parameter declarations &
	 * code...
	 */


    /**
    * @parameter property="ta.debug.mode"
    */
    protected String debug;
        
    /**
     * @parameter property="project.build.directory"
     */
    private File buildDirectory; 
    /**
     * @parameter property=project.compileClasspathElements
     */
    private List<String> compileCP;

    /**
     * @parameter property=project.testClasspathElements
     */
    private List<String> testCP;

    /**
     * @parameter property=project.build.outputDirectory
     */
    private File mainClasses;

    /**
     * @parameter property=project.build.testOutputDirectory
     */
    private File testClasses;
    
    /**
    * @parameter property="tf.test.suite"
    */
    private String testList;
    
    /*
     * Beginning of configurerd configuration related parameters section. As we want to avoid having to 
     * define in the pom of each project the configurers to be as transparent as possible, we
     * declare here all the possible parameters to instantiate a TMCallbackConfigurer.
     */
    
    /**
     * @parameter property="status.update.events.url"
     */
    private String endpointURL;
    
    /**
     * @parameter property="squash.ta.external.id"
     */
    private String externalExecutionId;

    /**
     * @parameter property="jobname"
     */    
    private String jobName;
    
    /**
    * @parameter property="hostname"
    */    
    private String hostName;
    
    /**
     * @parameter property="squash.ta.conf.file"
     */    
    private File endpointConfigurationFile;

    /**
     * @parameter property="ta.tmcallback.reportbaseurl"
     */        
    private String reportBaseUrl;
    
    /**
     * @parameter property="ta.tmcallback.jobexecutionid"
     */        
    private String jobExecutionId;
    
    /**
     * @parameter property="ta.tmcallback.reportname"
     */        
    private String reportName;
    /*
     * End of configurerd configuration related parameters section
     */
    
    @Override
    protected SuiteResult executeImpl() throws MojoExecutionException, MojoFailureException {
        /* mojo's ... mojos ;) */
        init();                      
        initExporters();
        initConfigurers();
        resetOutputDirectory();
       
        
        /* project analysis & test suite generation */        
        final JunitTestBundle mainCodeBundle = new JunitTestBundle(new ThirdPartyJavaCodeBundle(compileCP,mainClasses));
        final JunitTestBundle testCodeBundle = new JunitTestBundle(new ThirdPartyJavaCodeBundle(testCP,testClasses));
        Junit5TestWorkspaceBrowser browser = new Junit5TestWorkspaceBrowser(mainCodeBundle, testCodeBundle);
                
        List<TestPointer> testSpecs = new JunitTestSpecparser(testList, browser).parseTestList();
        
        //Retrieve SuiteSpecification that will be used by ParametersFactory to get all test params and global params to TFParamService
        SuiteSpecification suiteSpecs = new JunitTestSpecparser(testList, browser).parseTestSuite();
        ParametersFactory paramFactory = new ParametersFactory(suiteSpecs);
        TFParamServiceImpl.registerTFParamServiceImpl(paramFactory);
                
        TestPointerManager testPointerManager = new TestPointerManager(testSpecs);
        //group tests by its class
        Map<String, List<TestPointer>> testGroupMap = testPointerManager.groupPointersByEcosystemDefinition();
        //metadata content validation
        new JunitRunnerMetadataSyntaxChecker(testGroupMap, false, false, false).checkMetadataSyntaxByDefault();
                
        //here for test filtering code, we'll let that aside for now
        //for the time being, ignore it and go for full test set execution
        final Junit5TestSuiteFactory testSuiteFactory = Junit5TestSuiteFactoryBuilder.create(Junit5TestSuiteFactory.class);
        TestSuite testSuite = testSuiteFactory.generateTestSuite(testSpecs);
        /* Engine load & test suite execution */

        //retrieve the project group ID, artifact ID and version from its pom.xml
        MavenXpp3Reader reader = new MavenXpp3Reader();
        try {
            Model model = reader.read(new FileReader("pom.xml"));
            testSuite.setProjectDescription(model.getGroupId(), model.getArtifactId(), model.getVersion());
        
            EngineLoader loader=new EngineLoader();
            Engine engine = loader.load();
            applyConfigurers(engine);
            
            return engine.execute(testSuite, browser);
        } catch (FileNotFoundException ex) {
            getLogger().debug("pom.xml file not found : "+ex);
        } catch (IOException | XmlPullParserException ex) {
            getLogger().debug("problem encoutered while parsing the pom.xml file : "+ex);
        }
        
        return null;
    }

    private void init(){
        if(getOutputDirectory()==null) {
            setOutputDirectory(new File(buildDirectory, "squashTF"));
        }
    
        if(testList==null) {
            testList = "**/*";
        }        
    }

    private void initExporters() {
        getLogger().debug("Registering engine configurers");
        final TAMojoExporterAdapter adapter = new TAMojoExporterAdapter(this);
        
        ResultExporter simpleExporter = new HtmlSummarySuiteResultExporter();
        
        ResultExporter detailedExporter = new HtmlSuiteResultExporter();
        detailedExporter.setOutputDirectoryName("html-details");
        
        adapter.registerResultExporters(simpleExporter, detailedExporter);
    }
    

    
    private void initConfigurers() {
            TestInfoEventConfigurer testInfoConfigurer = new TestInfoEventConfigurer();
            setConfigurers(new Configurer[] {testInfoConfigurer});
            
            if(endpointURL == null  || "".equals(endpointURL.trim())
              ) {
                getLogger().debug("Local execution, no configurers will be provided");
                return;
            }
            
            try {
                getLogger().debug(createInitConfigurerMessage());
                
                TmCallBack configurer = new TmCallBack();
                configurer.setEndpointURL(new URL(endpointURL));
                configurer.setExecutionExternalId(externalExecutionId);
                configurer.setJobName(jobName);
                configurer.setHostName(hostName);
                configurer.setEndpointLoginConfFile(endpointConfigurationFile);
                configurer.setBaseReportUrl(reportBaseUrl);
                configurer.setJobExecutionId(jobExecutionId);
                configurer.setReportName(reportName);
                
            
                setConfigurers(new Configurer[] {configurer,testInfoConfigurer});
            } catch (MalformedURLException ex) {
                getLogger().warn("Engine configurers initialization failed. Report may not be transmitted to Squash TM.",ex);
            }
    }

    private String createInitConfigurerMessage() {
        StringBuilder messageBuilder = new StringBuilder("Initializing engine configurers :\n")
                .append("Job name: \t").append(nullAppend(jobName)).append("\n")
                .append("Host name : \t").append(nullAppend(hostName)).append("\n")
                .append("Job execution ID: \t").append(nullAppend(jobExecutionId)).append("\n")
                .append("Report name ID: \t").append(nullAppend(reportName)).append("\n")
                .append("Base report URL : \t").append(nullAppend(reportBaseUrl)).append("\n")
                .append("Enpoint URL : \t").append(nullAppend(endpointURL)).append("\n")
                .append("Execution external ID : \t").append(nullAppend(externalExecutionId)).append("\n");
        if (endpointConfigurationFile == null) {
            messageBuilder.append("Endpoint configuration file : \t {null}\n");
        } else {
            try {
                messageBuilder.append("Endpoint configuration file : \t").append(endpointConfigurationFile.getCanonicalPath()).append("\n");
            } catch (IOException ex) {
                messageBuilder.append("Endpoint configuration file :  \t").append(endpointConfigurationFile.getAbsolutePath()).append("\n");
                getLogger().debug("Could not exctract canonical path. Printing absolute path instead",ex);
            }
        }
        return messageBuilder.toString();
    }

    private String nullAppend(String nullableString) {
        return (nullableString == null) ? "{null}" : nullableString;
    }
    @Override
    protected ReportType getReportType() {
        return ReportType.EXECUTION;
    }
    
        /*
	 * Workaround method code...
	 */
        @Override
	public void setConfigurers(Configurer[] configurerTable) {
		super.setConfigurers(configurerTable);
		configurers=configurerTable;
	}	
    
	@Override
	public void setExporters(ResultExporter[] exporters) {
		super.setExporters(exporters);
		this.exporters=exporters;
	}
        
	@Override
	public void setOutputDirectory(File outputDirectory) {
		super.setOutputDirectory(outputDirectory);
		this.outputDirectory=outputDirectory;
	}
        
}
