/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.tf.selenium.runner;

import java.net.URL;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class GridTests {

    @Test
    public void BasicTest() throws Exception {
        //example: use chrome as webdriver in Windows 10
        //first download the selenium-server-standalone-x.xx.xx.jar from:
        //https://docs.seleniumhq.org/download/
        //then to set up a testing environment for Selenium Grid2, follow the instructions in:
        //https://www.guru99.com/introduction-to-selenium-grid.html
        
        DesiredCapabilities cap = DesiredCapabilities.chrome();
        cap.setBrowserName("chrome");
        cap.setPlatform(Platform.WIN10);
        
        String baseUrl = "https://www.squashtest.org/fr";
        //TODO: modify the ip adrress-port for the hub 
        String nodeUrl = "http://your-ip:your-port/wd/hub";
        WebDriver driver = new RemoteWebDriver(new URL(nodeUrl), cap);
        
        driver.get(baseUrl);
        Thread.sleep(3000);
        driver.quit();
    }
}
