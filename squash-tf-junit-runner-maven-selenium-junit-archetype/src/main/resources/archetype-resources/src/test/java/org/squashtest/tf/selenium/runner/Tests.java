/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.tf.selenium.runner;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Tests {

    @Test
    public void BasicTest() throws Exception {
        //example: use chrome as webdriver
        //TODO: get the path to your chromedriver
        String driverPath = "path-to-chromedriver";
        System.setProperty("webdriver.chrome.driver", driverPath);
        
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.squashtest.org/fr");
        driver.manage().window().maximize();
        Thread.sleep(3000);
        driver.quit();
    }
}
