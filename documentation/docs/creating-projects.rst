..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#################
Creating projects
#################

**********************
Starting a new project
**********************

In case you are starting a new Maven Java Junit project from scratch, a Maven archetype is provided to help
you generate a correctly structured project with recommended dependencies. The archetetype group Id is 
``org.squashtest.ta.galaxia``, the archetype artifact Id is ``squash-tf-junit-runner-project-achetype`` and 
the current version is ``1.0.0-RELEASE``. Hence, to use it simply run the following command : 

.. code-block:: shell

   mvn archetype:generate -DarchetypeGroupId=org.squashtest.ta.galaxia -DarchetypeArtifactId=squash-tf-junit-runner-project-archetype -DarchetypeVersion=1.0.0-RELEASE

******************************************************
Tests implementation: sample with SoapUI Smartbear Api 
******************************************************

Once your project is created, you just have to implement your tests as Junit tests according to the version of Junit chosen.

Here is an example to run a REST test with the open source API proposed by SoapUI Smartbear.

We used: 

*  a REST test given in the tutorial installed with version 5.5.0 of SoapUI Smartbear software.
* the open source SoapUI Smartbear API version 5.1.3
*  the TF JUnit runner version 1.0.0-RELEASE
*  junit (jupiter) version 5.2.0


We have set in the POM's project the version of junit chosen:

.. image:: ./_static/images/soapuiSample/jupiterVersion.png
   :scale: 100 %
   :align: center   
   
You need to add the external dependency the SoapUI API in your POM file as follows:

.. image:: ./_static/images/soapuiSample/dependencies.png
   :scale: 100 %
   :align: center
   
In this example, you must also add in your POM a block for the Smartbear repository:

.. image:: ./_static/images/soapuiSample/repository.png
   :scale: 100 %
   :align: center

Create your test class and implement it. Your project now looks like this:

.. image:: ./_static/images/soapuiSample/code.png
   :scale: 45 %
   :align: center
   
Launch the TF junit runner:

.. image:: ./_static/images/soapuiSample/mavenGoal.png
   :scale: 90 %
   :align: center 

The HTML results file of the execution ``squash-tf-report.html`` is available in the subdirectory squashTF/html-report of the target folder.

.. image:: ./_static/images/soapuiSample/htmlreport.png
   :scale: 100 %
   :align: center 
   
Note: If you have not started the server mock, as indicated in the SoapUI tutorial, your test will fail on a connection error.
