..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

###########################
Squash TF Java Junit Runner
###########################

.. toctree::
   :hidden:
   :maxdepth: 2

   <Squash TF Doc Homepage> <https://squash-tf.readthedocs.io>
   Runner functions <runner-functions.rst>
   Junit tests reference scheme <junit-test-ref-scheme.rst>
   METADATA in JUnit Runner <metadata-in-junit-runner.rst>
   TF Param Service <tf-param-service.rst>
   Creating projects <creating-projects.rst>

********
Overview
********

The **Squash T**\ est **F**\ actory (|squashTF|) Java Junit Runner aims to
provide a seamless integration to our ecosystem when automated test are already
(will be) implemented using Java as a language and Junit as the underlying test
framework.

As a |squashTF|  runner its main goal is twofold :

* List in JavaScript Object Notation (Json) format the available implemented tests
* Run a selection (that can include all available tests) and report the execution.
  In the case where the execution order originates from **Squash T**\ est
  **M**\ anagement (|squashTM|), test status and report are sent back to
  |squashTM|.

The actual implementation of the Runner is based on the Maven technology. More
precisely each major feature ("List" and  "Run") is provided via an implementation
of a Maven Mojo.



