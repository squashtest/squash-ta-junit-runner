..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.


################
TF Param Service
################

This service allows you to retrieve the values of the parameters that are present in a **.json** file in order to use them in your JUnit test. 

.. warning::

   TF Param Service will only work properly with 1.2.0-RELEASE (or newer) version of TF JUnit Runner.

*************
Configuration
*************

In order to be able to use TF param Service in your JUnit project, you need to add a new dependency to your project.

**Add the following dependency to the pom.xml of your project :**

   .. code-block:: xml

        <dependency>
            <groupId>org.squashtest.ta.galaxia</groupId>
            <artifactId>tf-param-service</artifactId>
            <version>1.0.0-RELEASE</version>
        </dependency>

****************
Call the service
****************

If you want to use methods of the service in your JUnit test, you first need to call it. To do so, write the following:

   .. code-block:: java

        TFParamService.getInstance().[methodName()];

You will need to write this everytime you want to use the service.


*****************
Available methods
*****************

    * **getTestParam(String paramName) :** Returns a String of the value associated with the "paramName" parameter in the params section of the test in the supplied **.json** file. Returns << null >> if it can not be found.

    * **getTestParam(String paramName, String defaultValue) :** Returns a String of the value associated with the "paramName" parameter in the params section of the test in the supplied **.json** file. Returns defaultValue if it can not be found.

    * **getParam(String paramName) :** Search the "paramName" parameter in the test params section of the test in the supplied **.json** file. If the service does not find it, it looks in the global-params section. Returns a String of the result of its search or << null >> if nothing was found.

    * **getParam(String paramName, String defaultValue) :** Search the "paramName" parameter in the test params section of the test  in the supplied **.json** file. If the service does not find it, it looks in the global-params section. Returns a String of the result of its search or defaultValue if nothing was found.

    * **getGlobalParam(String paramName) :** Returns a String of the value associated with the "paramName" parameter in the global-params section of the supplied **.json** file. Returns << null >> if it can not be found.

    * **getGlobalParam(String paramName, String defaultValue) :** Returns a String of the value associated with the "paramName" parameter in the global-params section of the supplied **.json** file. Returns the defaultValue if it can not be found.

***************************
Manually provide .json file
***************************

If you want to manually provide the **.json file**, you need to add the following parameter **-Dtf.test.suite={file:path/to/json/FileName.json}** to your maven goal. 

**"path/to/json/FileName.json"** must be the relative path of your **.json file** from the root of your project.

If the **.json file** is located directly at the root of your project, just type **-Dtf.test.suite={file:FileName.json}**

**For example :**

   .. code-block:: none

        mvn clean compile test-compile org.squashtest.ta.galaxia:squash-tf-junit-runner-maven-plugin:1.2.0-RELEASE:run -Dtf.test.suite={file:testSuite.json}

**Example of .json file :**

.. code-block:: json

    {
        "test": [{
                "id": "39",
                "script": "maven.test.bundle:org.squashtest.tf.other.OtherExampleTest/Other Jupiter Test Display Name",
                "param": {
                    "TC_REFERENCE": "",
                    "TC_CUF_CUF_CUSTOM": "true",
                    "TC_UUID": "3a7099ff-ab59-4e99-b21d-07e7d71d1ed5"
                }
            }, {
                "id": "40",
                "script": "maven.test.bundle:org.squashtest.tf.other.OtherExampleTest/Other Jupiter Test Display Name",
                "param": {
                    "TC_REFERENCE": "",
                    "DS_name": "Bertrand",
                    "DSNAME": "dataset1",
                    "TC_CUF_CUF_CUSTOM": "true",
                    "DS_age": "41",
                    "TC_UUID": "adec6164-5dec-4c8c-a0ae-c036340d519b"
                }
            }, {
                "id": "41",
                "script": "maven.test.bundle:org.squashtest.tf.other.OtherExampleTest/Other Jupiter Test Display Name",
                "param": {
                    "TC_REFERENCE": "",
                    "DS_name": "Damien",
                    "DSNAME": "dataset2",
                    "TC_CUF_CUF_CUSTOM": "true",
                    "DS_age": "undefined",
                    "TC_UUID": "adec6164-5dec-4c8c-a0ae-c036340d519b"
                }
            }],
        "param": {
            "globalParamSection": "This is global param section",
            "user": "foo",
            "ow_ner.Na-me": "bar",
        }
    }       
    

