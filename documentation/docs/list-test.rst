..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

############################
List implemented Junit tests
############################

This Mojo enables one to list as a Json file the available implemented tests. In
order to do so one only needs to run the following command :

.. code-block:: shell

  mvn clean compile test-compile org.squashtest.ta.galaxia:squash-tf-junit-runner-maven-plugin:1.0.0-RELEASE:list

The command is structured as follows

* ``mvn`` : launch Maven
* ``clean`` : (Optional) One of Maven default goals that enables one to clean everything that
  has been previously build by Maven.
* ``compile`` : One of Maven default goals that enables one to compile code that is
  stored in src/main.
* ``test-compile`` : One of Maven default goals that enables one to compile code that is
  stored in src/test.
* ``org.squashtest.ta.galaxia:squash-tf-junit-runner-maven-plugin:1.0.0-RELEASE:list``  :
  the actual listing Mojo provided by the |squashTF| Java Junit Runner. It lists
  all Junit tests that can be discovered (in the Junit sense) in the code compiled
  during the "compile" and "test-compile" phases.

The result should be a Json file named testTree.json located at target/squashTF/test-tree.
It is a simple JavaScript table listing available test grouped by "ecosystems".
Ecosystem naming follows the convention explained in the :ref:`junit-test-ref-scheme`
section above.

*************************
'list' goal with Metadata
*************************

If there are metadata in the current test project, the goal **"list"** searches and checks if all metadata in this JUnit project respect the conventions for writing and using Squash TF metadata.
(See :ref:`metadata-in-junit-runner` for more information about Metadata syntax conventions)

The goal will check through the project, collect all the metadata error(s) if any and lead to the *FAILURE*. Otherwise, a *SUCCESS* result will be obtained.

Metadata error(s), if found, will be grouped by test names.

.. image:: ./_static/images/list-failure.png
   :align: center
   :alt: List build FAILURE

|

**************************************
Listing test JSON report with Metadata
**************************************

If the build is successful, the generated report (JSON file) will contain the metadata associated with each of the test scripts.

.. code-block:: json

    {
	    "timestamp": "2019-09-23T14:56:24.761+0000",
	    "name": "tests",
	    "contents": [{
			"name": "maven.test.bundle:com.example.project.CalculatorTest2",
			"contents": [{
			    "name": "testAdd()",
                "metadata": {},
                "contents": null
            }, {
                "name": "This is my test",
                "metadata": {
                    "key1..": null
                },
                "contents": null
            }, {
                "name": "testAddWithTFMetadata2()",
                "metadata": {
                    "key2": ["value2"]
                },
                "contents": null
            }, {
                "name": "testAddWithTFMetadata3()",
                "metadata": {
                    "Key3-": null,
                    "key_4": ["value4"],
                    "Key53": ["value4-","value4-"]
                },
                "contents": null
            }
        ]
    }

.. note::

   To ignore thoroughly metadata during the listing process as well as in the report, insert :guilabel:`tf.disableMetadata` property after the goal "list".

   .. code-block:: shell

     mvn clean compile test-compile org.squashtest.ta.galaxia:squash-tf-junit-runner-maven-plugin:1.0.0-RELEASE:list -Dtf.disableMetadata=true
	 
   or as a property in the **pom.xml** file
   
   .. code-block:: xml

     <properties>
	     <tf.disableMetadata>true</tf.disableMetadata>
     </properties>
 
