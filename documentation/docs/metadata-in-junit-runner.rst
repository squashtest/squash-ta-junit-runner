..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _metadata-in-junit-runner:

########################
Metadata in JUnit runner
########################

In your JUnit project, you can insert Squash Metadata into test methods via an Annotation named :guilabel:`TFMetadata`.
For example:

.. code-block:: java

    @Test
    @DisplayName("This is my test with Squash Metadata")
    @TFMetadata(key ="key", value ={"value"})
    public void testAddWithTFMetadata1() {
        Calculator calculator = new Calculator();
        assertEquals(2, calculator.add(1, 1), "1 + 1 should equal 2");
    }


******************************
Configuration : new dependency
******************************

In order to be able to use TF metadata in your Junit project, you have to add a new dependency to your project.

To do so :

#. Add to the ``dependencies`` section of your project pom.xml the following dependency :

   .. code-block:: xml

      <dependency>
        <groupId>org.squashtest.ta.galaxia</groupId>
        <artifactId>squash-tf-galaxia-annotations</artifactId>
        <version>1.0.0-RELEASE</version>
      </dependency>

#. Add to the ``repositories`` section (or create it if doesn't exist) of your project pom.xml the following repository. (This is needed because our dependency is not available on maven central but only in our repository)

   .. code-block:: xml

      <repositories>
        <repository>
          <id>org.squashtest.ta.release</id>
          <name>squashtest test automation - releases</name>
          <url>http://repo.squashtest.org/maven2/releases</url>
        </repository>
      </repositories>


***************************
Metadata syntax conventions
***************************

In a Metadata annotation, the **key** is mandatory.
A metadata key MUST be ONE WORD which contains only *alphanumeric characters, dashes, underscores and dots*. Spaces/tabulation are allowed before and after the word.

Moreover, metadata key is **case insensitive** and must be **unique** in a test file, even a test method.

.. code-block:: java

    @Test
    @DisplayName("For a typical Metadata Key")
    @TFMetadata(key ="   Key.01-is_Insensitive-and_MUST-be_UNIQUE      ", value ={"value"})
    public void testAddWithTFMetadata2() {
        Calculator calculator = new Calculator();
        assertEquals(3, calculator.add(1, 2), "1 + 2 should equal 3");
    }

On the other hand, the **value** is optional. However, a Metadata value, if it exists, MUST be ONE WORD containing only *alphanumeric characters, dashes, slashes, underscores and dots*.
Spaces/tabulation are also allowed before and after the word.

Metadata value is **case sensitive** and must be assigned to a metadata key. It is also possible to have many metadata values associated to a same key.

.. code-block:: java

    @Test
    @TFMetadata(key ="key1")
    public void testAddWithTFMetadata3() {
        Calculator calculator = new Calculator();
        assertEquals(4, calculator.add(1, 3), "1 + 3 should equal 4");
    }

    @Test
    @TFMetadata(key ="key2", value ={})
    public void testAddWithTFMetadata4() {
        Calculator calculator = new Calculator();
        assertEquals(5, calculator.add(1, 4), "1 + 4 should equal 5");
    }

    @Test
    @TFMetadata(key ="key3", value ={"   pathTo/Value.03-is_Sensitive      "})
    public void testAddWithTFMetadata5() {
        Calculator calculator = new Calculator();
        assertEquals(6, calculator.add(1, 5), "1 + 5 should equal 6");
    }

    @Test
    @TFMetadata(key ="key4", value ={"value1", "Value2", "value3"})
    public void testAddWithTFMetadata6() {
        Calculator calculator = new Calculator();
        assertEquals(7, calculator.add(1, 6), "1 + 6 should equal 7");
    }

A test method can be associated with from zero to many Metadata.

.. code-block:: java

    @Test
    @DisplayName("No metadata")
    public void testAddWithTFMetadata7() {
        Calculator calculator = new Calculator();
        assertEquals(8, calculator.add(1, 7), "1 + 7 should equal 8");
    }

    @Test
    @DisplayName("With one metadata")
    @TFMetadata(key ="key", value ={"value"})
    public void testAddWithTFMetadata8() {
        Calculator calculator = new Calculator();
        assertEquals(9, calculator.add(1, 8), "1 + 8 should equal 9");
    }

    @Test
    @DisplayName("With many metadata")
    @TFMetadata(key ="key1", value ={})
    @TFMetadata(key ="key2", value ={"value"})
    @TFMetadata(key ="key3", value ={"value1", "value2"})
    public void testAddWithTFMetadata9() {
        Calculator calculator = new Calculator();
        assertEquals(10, calculator.add(1, 9), "1 + 9 should equal 10");
    }

.. important::

   Please ensure that all Metadata keys in every JUnit method of a Test script are unique.

*********************************
Use metadata for TM - TF autolink
*********************************

TF metadata handles the TM - TF autolink. (TM - TF autolink is available since **TM 1.20.0** and **Java Junit Runner 1.1.0**)
Autolink is a feature to ease the link between a TM test case and a test automation script.
On TM side, a UUID is now provided (when the workflow is activated) :

.. image:: ./_static/images/metadata/autolink-tm-uuid.png
   :align: center

This UUID is used as an identifier.

In your automation test add a TF Metadata which key is ``linked-TC`` and value is the UUID from the corresponding TM test case.
As you can see in the example below, it's possible to link many TM test case to the same automation test (two UUID are set in the "value"):

.. image:: ./_static/images/metadata/linked-tc.png
   :align: center