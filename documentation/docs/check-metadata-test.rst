..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

############################
Junit test Metadata Checking
############################

As the goal **"list"**, the goal **check-metadata** searches and checks if all metadata in this JUnit project respect the conventions for writing and using Squash TF metadata.
(See :ref:`metadata-in-junit-runner` for more information about Metadata syntax conventions)

.. code-block:: shell

     mvn clean compile test-compile org.squashtest.ta.galaxia:squash-tf-junit-runner-maven-plugin:1.1.0-RELEASE:check-metadata

The goal will check through the project, collect all the metadata error(s) if any and lead to the *FAILURE*. Otherwise, a *SUCCESS* result will be obtained.
(However, no JSON report will be created with a successful **check-metadata** goal.)

Metadata error(s), if found, will be grouped by test names.

.. image:: ./_static/images/check-metadata-failure.png
   :align: center
   :alt: Check-metadata build FAILURE

When a JUnit project has *duplicate values* ​​in a *multi-value key* on a given test, the **check-metadata** goal will create a **WARNING** message in the console.

.. image:: ./_static/images/check-metadata-warning.png
   :align: center
   :alt: Check-metadata build WARNING

|

*******************************************
'check-metadata' goal with Unicity checking
*******************************************

In addition to the normal syntax checking, you can insert the :guilabel:`tf.metadata.check` property after the goal "check-metadata" to check the unicity of each Metadata Key - Value pair.

.. code-block:: shell

     mvn clean compile test-compile org.squashtest.ta.galaxia:squash-tf-junit-runner-maven-plugin:1.1.0-RELEASE:check-metadata -Dtf.metadata.check=[valueUnicity]

If there are metadata Key - Value duplicate(s) existed in the SKF project (even if the syntax is OK), a *FAILURE* result will be obtained.

.. image:: ./_static/images/check-metadata-with-unicity-failure.png
   :align: center
   :alt: Check-metadata with Unicity checking build FAILURE

|

'check-metadata' goal with Unicity checking for specific Keys
*************************************************************
You can even check the unicity of each metadata Key - Value pair with just some specific Keys by inserting the second property :guilabel:`tf.metadata.check.key` after the first one mentioned above.

.. code-block:: shell

     mvn clean compile test-compile org.squashtest.ta.galaxia:squash-tf-junit-runner-maven-plugin:1.1.0-RELEASE:check-metadata -Dtf.metadata.check=[valueUnicity] -Dtf.metadata.check.keys=[xxx,yyy,zzz]

.. important::

     In the bracket, the key list MUST be a string of characters composed by concatenation from 1 to n keys separated by commas: -Dtf.metadata.check.keys=[xxx,yyy,zzz]

     If the list is surrounded by double quotes, spaces are allowed: -Dtf.metadata.check.keys="[xxx, yyy, zzz]"

     It is NOT allowed to have two commas without any key OR only spaces/tabulations between them. (ex: -Dtf.metadata.check.keys="[xxx,     ,yyy,,zzz]")

     Key list is NOT allowed to be either uninitiated or empty. (ex: -Dtf.metadata.check.keys= OR -Dtf.metadata.check.keys=[])

For each searched metadata key, if there are Key - Value duplicate(s) existed in the SKF project, a *FAILURE* result will be obtained.

.. image:: ./_static/images/check-metadata-with-unicity-limited-failure.png
   :align: center
   :alt: Check-metadata with Unicity checking for specific Keys build FAILURE

.. note::
   If searched metadata key(s) are not found in any Test files, a **WARNING** message will be raised in the console.

   .. image:: ./_static/images/missing-key.png
      :align: center
      :alt: Missing Key