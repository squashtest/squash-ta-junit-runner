..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

################
Runner Functions
################

The runner is transparent and should be able to run any Maven Junit 4 or 5 project
**without any need to modify it**\ . Indeed the Mojos are implemented in such
a way that they are fully autonomous and do not need any project specific configuration
to run.

.. toctree::
    :titlesonly:
    :maxdepth: 2

    Junit test Listing <list-test.rst>
    Junit test Running <run-test.rst>
    Junit test METADATA Checking <check-metadata-test.rst>
