..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

##################
Junit test Running
##################

This Mojo enables one to run a selection of, or all possible, Junit tests and report
their execution. In order to do so one only needs to run the following command :

.. code-block:: shell

  mvn clean compile test-compile org.squashtest.ta.galaxia:squash-tf-junit-runner-maven-plugin:1.0.0-RELEASE:run

* ``mvn`` : launch Maven
* ``clean`` : (Optional) One of Maven default goals that enables one to clean everything that
  has been previously build by Maven.
* ``compile`` : One of Maven default goals that enables one to compile code that is
  stored in src/main.
* ``test-compile`` : One of Maven default goals that enables one to compile code that is
  stored in src/test.
* ``org.squashtest.ta.galaxia:squash-tf-junit-runner-maven-plugin:1.0.0-RELEASE:run``  :
  the actual running Mojo provided by |squashTF| Java Junit Runner. It runs
  the Junit tests that can be discovered (in the Junit sense) in the code compiled
  during the "compile" and "test-compile" phases.

By default the whole collection of tests available in the project will be executed.
A summary of the execution is reported and available at
target/squashTF/html-reports/squash-tf-report.html . A more detailed version of the
report providing context in the case of technical error is also produced and
available at target/squashTF/html-details/squash-tf-report.html. Finally a surfire
report is also produced and available at target/squashTF/surefire-reports/ .

If one wants to only run a subset of possible test one can provide a list of tests
via the Maven property "tf.test.suite". Two mechanism are possible:

* Mimic TM-TF link and provide a list of selected test via a Json file. In this
  scenario the tf.test.suite parameter should be given the value "{file:testsuite.json}"
  and the testsuite.json should be put right to the project pom.
* Provide a CSV like line, where qualified tests names are listed separated by semicolons

In both cases test convention should follow the one used by the listing Mojo and described
in the :ref:`junit-test-ref-scheme` section above.

.. warning::
   If there are metadata syntax errors in the running test script(s), **warning** message(s) will be displayed in the console.
   (See :ref:`metadata-in-junit-runner` for more information about Metadata syntax conventions)