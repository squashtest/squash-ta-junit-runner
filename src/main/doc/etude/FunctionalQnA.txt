====
        This file is part of the Squashtest platform.
        Copyright (C) 2018 - 2020 Henix

        See the NOTICE file distributed with this work for additional
        information regarding copyright ownership.

        This is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        this software is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this software.  If not, see <http://www.gnu.org/licenses />.
====

1) granularity is : 1 junit <=> 1 TA ?
2) what of repeted tests => 1 TA?
				+ simple mapping
				- grouping results in reports ?
			 => n TA ?
			 	+ result grouping simple
			 	- what does it mean in terms of mapping
3) test class => Ecosystem ?
